
/***********/
 YouTube CCK
/***********/

Author: Aaron Winborn
Original Development
2007-02-23

Requires: Drupal 5, cck
Optional: Views

This module will install CCK fields for YouTube Video. It allows
video to be displayed in a preview or full size, each of configurable
sizes. Also will grab the thumbnails from YouTube API.

Questions can be directed to winborn at advomatic dot com

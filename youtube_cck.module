<?php

define('YOUTUBE_CCK_REST_ENDPOINT', 'http://www.youtube.com/api2_rest');

function youtube_cck_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/youtube_cck',
      'title' => t('YouTube CCK configuration'),
      'description' => t('Configure YouTube CCK API key. Necessary if you wish to display YouTube thumbnails with the videos.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'youtube_cck_settings',
      'access' => user_access('administer site configuration'));
    $items[] = array(
      'path' => 'admin/settings/youtube_cck/configure',
      'title' => t('Configure'),
      'description' => t('Configure YouTube CCK API key. Necessary if you wish to display YouTube thumbnails with the videos.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'youtube_cck_settings',
      'access' => user_access('administer site configuration'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/settings/youtube_cck/convert',
      'title' => t('Convert'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'youtube_cck_convert_settings',
      'access' => user_access('administer site configuration'),
      'type' => MENU_LOCAL_TASK,
    );
  }
  return $items;
}

function youtube_cck_settings() {
  drupal_set_message(t('The @youtube module is deprecated. It is highly recommended that you install the !emfield module and its included @emvideo module, and convert any @youtube types from the !conversion.',
    array(
      '@youtube' => 'YouTube CCK',
      '!emfield' => '<a href="http://drupal.org/project/emfield">Embedded Media Field</a>',
      '@emvideo' => 'Embedded Video Field',
      '!conversion' => l(t('conversion page'), 'admin/settings/youtube_cck/convert'),
    )), 'error');

  // link to http://www.youtube.com/my_profile_dev for api code
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('YouTube API'),
    '#description' => t('If you wish to be able to display YouTube thumbnails automatically, you will first need to apply for an API Developer Key from the !youtube. Note that you do not need this key to display YouTube videos themselves.', array('!youtube' => l('YouTube Developer Profile page', 'http://www.youtube.com/my_profile_dev', array('target' => '_blank')))),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['api']['youtube_cck_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('YouTube API Key'),
    '#default_value' => variable_get('youtube_cck_api_key', ''),
    '#description' => t('Please enter your YouTube Developer Key here.'),
  );
  $form['api']['youtube_cck_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('YouTube API Secret'),
    '#default_value' => variable_get('youtube_cck_api_secret', ''),
    '#description' => t('If you have a secret for the YouTube API, enter it here.'),
  );
  return system_settings_form($form);
}

/**
 * Submit a request to YouTube. Borrowed & modified from the flickr.module
 * call like: youtube_cck_request('youtube.videos.get_details', array('video_id' => '0AFjIUhGvmI'));
 *
 * @param $method
 *   string method name
 * @param $args
 *   associative array of arguments names and values
 * @param $cacheable
 *   boolean indicating if it's safe cache the results of this request
 *
 * @return
 *   the result of the request
 */
function youtube_cck_request($method, $args = array(), $cacheable = TRUE) {
  $args['dev_id'] = trim(variable_get('youtube_cck_api_key', ''));
  $args['method'] = $method;
  ksort($args);

  // build an argument hash that we'll use for the cache id and api signing
  $arghash = '';
  foreach($args as $k => $v){
    $arghash .= $k . $v;
  }
  // if we've got a secret sign the arguments
  // TODO: doesn't seem to matter
//  if ($secret = trim(variable_get('youtube_cck_api_secret', ''))) {
//    $args['api_sig'] = md5($secret . $arghash);
//  }

  // build the url
  foreach ($args as $k => $v){
    $encoded_params[] = urlencode($k).'='.urlencode($v);
  }
  $url = YOUTUBE_CCK_REST_ENDPOINT .'?'. implode('&', $encoded_params);

  // if it's a cachable request, try to load a cached value
  if ($cacheable) {
    if ($cache = cache_get($arghash, 'cache')) {
      return unserialize($cache->data);
    }
  }

  // connect and fetch a value
  $result = drupal_http_request($url);

  if ($result->code == 200) {
    $parser = drupal_xml_parser_create($result->data);
    $vals = array();
    $index = array();
    xml_parse_into_struct($parser, $result->data, $vals, $index);
    xml_parser_free($parser);

    $params = array();
    $level = array();
    $start_level = 1;
    foreach ($vals as $xml_elem) {
      if ($xml_elem['type'] == 'open') {
        if (array_key_exists('attributes',$xml_elem)) {
          list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
        } else {
          $level[$xml_elem['level']] = $xml_elem['tag'];
        }
      }
      if ($xml_elem['type'] == 'complete') {
        $php_stmt = '$params';
        while($start_level < $xml_elem['level']) {
          $php_stmt .= '[$level['.$start_level.']]';
          $start_level++;
        }
        $php_stmt .= '[$xml_elem[\'tag\']][] = $xml_elem[\'value\'];';
        eval($php_stmt);
        $start_level--;
      }
    }

    // save a cacheable result for future use
    if ($cacheable) {
      cache_set($arghash, 'cache', serialize($params), time() + 3600);
    }
    return $params;
  }
  return array();
}

/**Implementation of hook_field_info  **/

function youtube_cck_field_info() {
  $fields = array(
    'youtube_cck_youtube' => array('label' => t('YouTube Video')),
  );
  return $fields;
}

/** Implementation of hook_field_settings **/

function youtube_cck_field_settings($op, $field) {
  switch ($op) {

    case 'database columns':
      $columns = array(
        'embed' => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => "''", 'sortable' => TRUE),
        'value' => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => "''", 'sortable' => TRUE),
      );
      switch ($field['type']) {
        case 'youtube_cck_youtube':
          break;
      }
      return $columns;
  }
}

/** Implementation of hook_field **/

function youtube_cck_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
  case 'view':
    foreach ($items as $delta => $item) {
      $items[$delta]['view'] = content_format($field, $item, 'default', $node);
    }
    return theme('field', $node, $field, $items, $teaser, $page);

  case 'validate':
    if ($field['multiple']) {
      foreach ($items as $delta => $item) {
        $error_field = $field['field_name'].']['.$delta.'][embed';

        _youtube_cck_field_validate_id($field, $item, $error_field);
      }
    }
    else {
      $error_field = $field['field_name'];

      _youtube_cck_field_validate_id($field, $items[0], $error_field);
    }
    break;

  case 'submit':
    if ($field['multiple']) {
      foreach ($items as $delta => $item) {
        $error_field = $field['field_name'].']['.$delta.'][embed';
        $items[$delta]['value'] = _youtube_cck_field_submit_id($item);
      }
    }
    else {
      $error_field = $field['field_name'];
      $items[0]['value'] = _youtube_cck_field_submit_id($items[0]);
    }
    break;
  }
}

/**
 *  extract the youtube id from embedded code or video url
 */
function _youtube_cck_field_validate_id($field, $item, $error_field) {
  return _youtube_cck_field_submit_id($item);
}

/**
 *  replace youtube embedded code with the extracted id. this goes in the field 'value'
 *  also allows you to grab directly from the URL to play the video
 */
function _youtube_cck_field_submit_id($item) {
  $embed = $item['embed'];

  // src="http://www.youtube.com/v/nvbQQnvxXDk"
  if ($embed && preg_match('@youtube\.com/v/([^"\&]+)@i', $embed, $matches)) {
    $item['value'] = $matches[1];
  }
  else if ($embed && preg_match('@youtube\.com/watch\?v=([^"\&]+)@i', $embed, $matches)) {
    $item['value'] = $matches[1];
  }
  else {
    $item['value'] = $embed;
  }
  return $item['value'];
}

/** Implementation of hook_field_formatter_info **/
function youtube_cck_field_formatter_info() {
  $types = array('youtube_cck_youtube',);
  $formats = array(
    'default' => array(
      'label' => t('Default'),
      'field types' => $types,
    ),
    'youtube_video' => array(
      'label' => t('YouTube Full Size Video'),
      'field types' => $types,
    ),
    'youtube_preview' => array(
      'label' => t('YouTube Preview Video'),
      'field types' => $types,
    ),
    'youtube_thumbnail' => array(
      'label' => t('YouTube Thumbnail'),
      'field types' => $types,
    ),
  );
  return $formats;
}

/** Implementation of hook_field_formatter **/

function youtube_cck_field_formatter($field, $item, $formatter, $node) {
  if (!isset($item['value'])) {
    return '';
  }

  // unfortunately, when we come from a view, we don't get all the widget fields
  if (!$node->type) {
    $type = content_types($field['type_name']);
    $field['widget'] = $type['fields'][$field['field_name']]['widget'];
  }

  switch ($formatter) {
    case 'youtube_thumbnail':
      $output .= theme('youtube_cck_thumbnail', $field, $item);
      break;
    case 'youtube_preview':
      $output .= theme('youtube_cck_preview', $field, $item);
      break;
    case 'youtube_video':
    case 'default':
    default:
      $output .= theme('youtube_cck_video', $field, $item);
      break;
  }
  return $output;
}

/** Widgets **/

/** Implementation of hook_widget_info **/
function youtube_cck_widget_info() {
  return array(
    'youtube_cck_textfields' => array(
      'label' => t('Text Fields'),
      'field types' => array('youtube_cck_youtube',),
    ),
  );
}

function youtube_cck_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      if ($widget['type'] == 'youtube_cck_textfields') {
        $form['video'] = array(
          '#type' => 'fieldset',
          '#title' => t('YouTube Video Settings'),
          '#description' => t('These settings control how this video is displayed in its full size, which defaults to 350x425.'),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['video']['video_height'] = array(
          '#type' => 'textfield',
          '#title' => t('YouTube video height'),
          '#default_value' => $widget['video_height'] ? $widget['video_height'] : 350,
          '#required' => true,
          '#description' => t('The height of the YouTube video. It defaults to 350.'),
        );
        $form['video']['video_width'] = array(
          '#type' => 'textfield',
          '#title' => t('YouTube video width'),
          '#default_value' => $widget['video_width'] ? $widget['video_width'] : 425,
          '#required' => true,
          '#description' => t('The width of the YouTube video. It defaults to 425.'),
        );

        $form['preview'] = array(
          '#type' => 'fieldset',
          '#title' => t('YouTube Video Settings'),
          '#description' => t('These settings control how this video is displayed in its full size, which defaults to 350x425.'),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['preview']['preview_height'] = array(
          '#type' => 'textfield',
          '#title' => t('YouTube preview height'),
          '#default_value' => $widget['preview_height'] ? $widget['preview_height'] : 350,
          '#required' => true,
          '#description' => t('The height of the YouTube preview video. It defaults to 350.'),
        );
        $form['preview']['preview_width'] = array(
          '#type' => 'textfield',
          '#title' => t('YouTube preview width'),
          '#default_value' => $widget['preview_width'] ? $widget['preview_width'] : 425,
          '#required' => true,
          '#description' => t('The width of the YouTube preview video. It defaults to 425.'),
        );

        $form['tn'] = array(
          '#type' => 'fieldset',
          '#title' => t('Thumbnail'),
          '#description' => t('These settings control what thumbnail to display from YouTube.'),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['tn']['tn_display'] = array(
          '#type' => 'select',
          '#title' => t('What YouTube Thumbnail to Display'),
          '#default_value' => $widget['tn_display'],
          '#options' => array(
            0 => t('Random Thumbnail'),
            1 => t('First Thumbnail'),
            2 => t('Second Thumbnail'),
            3 => t('Third Thumbnail'),
            'all' => t('All Thumbnails'),
          ),
        );
      }
      return $form;

    case 'validate':
      if ($widget['type'] == 'youtube_cck_textfields') {
        if (!is_numeric($widget['video_width']) || intval($widget['video_width']) != $widget['video_width'] || $widget['video_width'] < 1) {
          form_set_error('video_width', t('"Video Width" must be a positive integer.'));
        }
        if (!is_numeric($widget['video_height']) || intval($widget['video_height']) != $widget['video_height'] || $widget['video_height'] < 1) {
          form_set_error('video_height', t('"Video Height" must be a positive integer.'));
        }
        if (!is_numeric($widget['preview_width']) || intval($widget['preview_width']) != $widget['preview_width'] || $widget['preview_width'] < 1) {
          form_set_error('preview_width', t('"Preview Width" must be a positive integer.'));
        }
        if (!is_numeric($widget['preview_height']) || intval($widget['preview_height']) != $widget['preview_height'] || $widget['preview_height'] < 1) {
          form_set_error('preview_height', t('"Preview Height" must be a positive integer.'));
        }
      }
      break;

    case 'save':
      if ($widget['widget']['type'] == 'youtube_cck_textfields') {
        return array('video_width', 'video_height', 'preview_width', 'preview_height', 'tn_display', );
      }
      break;
  }
}

/** Implementation of hook_widget **/

function youtube_cck_widget($op, &$node, $field, &$node_field) {
  switch ($op) {
    case 'form':
      $form = array();

      $form['youtube_cck_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t($field['widget']['label']),
        '#description' => $field['widget']['description'],
        '#collapsible' => true,
        '#collapsed' => false,
      );

      $form['youtube_cck_fieldset'][$field['field_name']] = array('#tree' => TRUE);
      $textfield = 'embed';
      $field['required'] = FALSE;
      $textfield_title = t('YouTube Embed Code, Video URL, or Video ID');
      $textfield_description = t('Enter the Embed Code, Video URL, or Video ID here. For instance, you might enter the URL from the address bar, from the Embed bar provided by YouTube, or simply cut and paste the YouTube video code.');

      if ($field['multiple']) {
        $form['youtube_cck_fieldset'][$field['field_name']]['#type'] = 'fieldset';
        $form['youtube_cck_fieldset'][$field['field_name']]['#title'] = t($field['widget']['label']);
        $delta = 0;
        foreach ($node_field as $data) {
          if (isset($data[$textfield])) {
            $form['youtube_cck_fieldset'][$field['field_name']][$delta][$textfield] = array(
              '#type' => 'textfield',
              '#title' => $textfield_title,
              '#description' => $textfield_description,
              '#default_value' => $data[$textfield],
              '#required' => ($delta == 0) ? $field['required'] : FALSE,
              '#maxlength' => 255,
            );
            $form['youtube_cck_fieldset'][$field['field_name']][$delta]['value'] = array(
              '#type' => 'value',
              '#value' => $data['value'],
            );
            if ($data['value']) {
              $form['youtube_cck_fieldset'][$field['field_name']][$delta]['markup_value'] = array(
                '#type' => 'markup',
                '#value' => t('(YouTube Video ID: @value)', array('@value' => $data['value'])),
              );
            }
            $delta++;
          }
        }
        foreach (range($delta, $delta + 2) as $delta) {
          $form['youtube_cck_fieldset'][$field['field_name']][$delta][$textfield] = array(
            '#type' => 'textfield',
            '#title' => $textfield_title,
            '#description' => $textfield_description,
            '#default_value' => '',
            '#required' => ($delta == 0) ? $field['required'] : FALSE,
            '#maxlength' => 255,
          );
          $form['youtube_cck_fieldset'][$field['field_name']][$delta]['value'] = array(
            '#type' => 'value',
            '#title' => '',
          );
        }
      }
      else {
        $form['youtube_cck_fieldset'][$field['field_name']][0][$textfield] = array(
          '#type' => 'textfield',
          '#title' => $textfield_title, //t($field['widget']['label']),
          '#description' => $textfield_description,
          '#default_value' => isset($node_field[0][$textfield]) ? $node_field[0][$textfield] : '',
          '#required' => $field['required'],
          '#maxlength' => 255,
        );
        if ($textfield == 'embed') {
          $value = isset($node_field[0]['value']) ? $node_field[0]['value'] : '';
          $form['youtube_cck_fieldset'][$field['field_name']][0]['value'] = array(
            '#type' => 'value',
            '#value' => $value,
          );
          if ($value) {
            $form['youtube_cck_fieldset'][$field['field_name']][0]['value_markup'] = array(
              '#type' => 'markup',
              '#value' => t('(YouTube Video ID: @value)', array('@value' => $value)),
            );
          }
        }
      }
      return $form;
    default:
      break;
  }
}

function youtube_cck_thumbnail($youtube_id) {
  $request = youtube_cck_request('youtube.videos.get_details', array('video_id' => $youtube_id));
  return $request['THUMBNAIL_URL'][0];
}

function theme_youtube_cck_thumbnail ($field, $item) {
  if ($item['value']) {
    $tn = youtube_cck_thumbnail($item['value']);
    if ($tn) {
      return '<img src="' . $tn . '" />';
    }
  }
}

function theme_youtube_cck_video($field, $item) {
  $width = $field['widget']['video_width'];
  $height = $field['widget']['video_height'];
  $embed = $item['value'];
  $output = theme('youtube_cck_youtube_flash', $embed, $width, $height);
  return $output;
}

function theme_youtube_cck_preview($field, $item) {
  $width = $field['widget']['preview_width'];
  $height = $field['widget']['preview_height'];
  $embed = $item['value'];
  $output = theme('youtube_cck_youtube_flash', $embed, $width, $height);
  return $output;
}

function theme_youtube_cck_youtube_flash($embed, $width, $height) {
  if ($embed) {
    $output .= "<object height=\"$height\" width=\"$width\"><param name=\"movie\" value=\"http://www.youtube.com/v/$embed\"><param name=\"wmode\" value=\"transparent\"><embed src=\"http://www.youtube.com/v/$embed\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" height=\"$height\" width=\"$width\"></object>";
  }
  return $output;
}

/**
 *  This is the form that will convert all your youtube_cck types to emfield video types.
 */
function youtube_cck_convert_settings() {
  $remove_msg = t('You currently have no types using %youtube that need to be converted. You may safely disable and remove the %youtube module now. If you experienced any difficulties with this process, please !report.', array('%youtube' => 'YouTube CCK', '!report' => l(t('report your problems'), 'http://drupal.org/project/issues/youtube_cck')));
  $form = array();
  $form['convert'] = array(
    '#type' => 'fieldset',
    '#title' => t('Convert types'),
    '#description' => t('The following types are registered as belonging to the %youtubecck module. Pressing the Convert button will convert them to work with %emfield instead. After doing so, or if you don\'t see any types on this page, you can safely disable the %youtubecck module. Note that the fields themselves will be changed, but will keep any data already stored.', array('%youtubecck' => 'YouTube CCK', '%emfield' => 'Embedded Media Field')),
  );
  $system_types = _content_type_info();
  $content_types = $system_types['content types'];
  $field_types = $system_types['field types'];
  $types = array();
  foreach ($content_types as $content_type => $type) {
    // determine which content types implement this module
    foreach ($type['fields'] as $field_type => $field) {
      if ($field_types[$field['type']]['module'] == 'youtube_cck') {
        $types[$content_type][$field_type] = $field;
      }
    }
  }

  // if we have any video_cck types, then list them here
  if (!empty($types)) {
    drupal_set_message(t('The @youtube module is deprecated. It is highly recommended that you install the !emfield module and its included @emvideo module, and convert any @youtube types from this screen. PLEASE make sure to create a backup of your database in case anything goes wrong. If you experience any difficulties with this process, please !report.', array('@youtube' => 'YouTube CCK', '!emfield' => '<a href="http://drupal.org/project/emfield">Embedded Media Field</a>', '@emvideo' => 'Embedded Video Field', '!report' => l(t('report your problems'), 'http://drupal.org/project/issues/youtube_cck'))), 'error');
    foreach ($types as $type_name => $type) {
      $fields = array();
      // add a link for each field that uses youtube videos
      foreach ($type as $field_name => $field) {
        $fields[$field_name] = l($field['field_name'], 'admin/content/types/' . $type_name . '/fields/' . $field['field_name']);
      }
      // add a link to the content type edit page
      $options[$type_name] = l($content_types[$type_name]['name'], 'admin/content/types/' . $type_name) . ' (' . implode(', ', $fields) . ')';
    }

    //list all the types to be converted
    $form['convert']['types'] = array(
      '#type' => 'markup',
      '#value' => t('The following types will be converted: !types', array('!types' => theme('item_list', $options))),
    );

    // if we don't have emfield installed, then show a message here, and don't include a submit button
    if (!module_exists('emfield') || !module_exists('video_cck')) {
      drupal_set_message('You must have the !emfield module and its included @emvideo module active before converting types.', array('!emfield' => '<a href="http://drupal.org/project/emfield">Embedded Media Field</a>', '@emvideo' => 'Embedded Video Field'), 'error');
    }
    else {
      $form['convert']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Convert types'),
      );
    }
  }
  else {
    // we don't have any types. safe to disable this module now
    drupal_set_message($remove_msg);
    $form['convert']['na'] = array(
      '#type' => 'markup',
      '#value' => $remove_msg,
    );
  }

  // display a warning to change any relevant code
  drupal_set_message(t('Note that if you are using any custom code in template files to display videos in nodes, you may need to rewrite some code. Specifically, although the field names themselves will be untouched, the field formatters have changed. The formatter for youtube_video has become video_video, youtube_preview is now video_preview, and youtube_thumbnail is video_thumbnail.'), 'error');
  return $form;
}

/**
 *  convert all existing types that contain youtube_cck fields to emfield
 */
function youtube_cck_convert_settings_submit($form_id, $form_values) {
  if (!module_exists('emfield') || !module_exists('video_cck')) {
    drupal_set_message('You must have the !emfield module and its included @emvideo module active before converting types.', array('!emfield' => '<a href="http://drupal.org/project/emfield">Embedded Media Field</a>', '@emvideo' => 'Embedded Video Field'), 'error');
    return;
  }
  $system_types = _content_type_info();
  $content_types = $system_types['content types'];
  $field_types = $system_types['field types'];
  $types = array();
  $field_tables = array();

  // a list of display/view formatters that need to be converted
  $formatters = array(
    'youtube_video' => 'video_video',
    'youtube_preview' => 'video_preview',
    'youtube_thumbnail' => 'video_thumbnail',
  );

  // make a list of types to be converted
  foreach ($content_types as $content_type => $type) {
    // determine which content types implement this module
    foreach ($type['fields'] as $field_type => $field) {

      // if the type has a youtube_cck field, then mark it to be converted
      if ($field_types[$field['type']]['module'] == 'youtube_cck') {
        $types[$content_type][$field_type] = $field;
        if (!empty($field_tables[$field_type]) || $field['multiple']) {
          $field_tables[$field_type] = 'content_' . $field_type;
        }
        else {
          $field_tables[$field_type] = 'content_type_' . $content_type;
        }
      }
    }
  }

  // stop if no types for youtube_cck
  if (empty($types)) {
    drupal_set_message('You have no types using YouTube CCK that need to be converted.', 'error');
    return;
  }

  // set display settings and widget types
  foreach ($types as $type_name => $fields) {
    foreach ($fields as $field_name => $field) {
      $display_settings = unserialize(db_result(db_query_range("SELECT display_settings FROM {node_field_instance} WHERE type_name = '%s' AND field_name='%s'", $type_name, $field_name, 0, 1)));
      if (is_array($display_settings)) {
        foreach ($display_settings as $display => $formatter) {
          foreach ($formatters as $old => $new) {
            if ($formatter['format'] == $old) {
              $display_settings[$display]['format'] = $new;
            }
          }
        }
        db_query("UPDATE {node_field_instance} SET widget_type = 'video_cck_textfields', display_settings='%s' WHERE type_name = '%s' AND field_name = '%s'", serialize($display_settings), $type_name, $field_name);
      }
      else {
        db_query("UPDATE {node_field_instance} SET widget_type = 'video_cck_textfields' WHERE type_name='%s' AND field_name = '%s'", $type_name, $field_name);
      }
    }
  }

  // change node content tables
  foreach ($field_tables as $field_name => $table) {
    db_query("UPDATE {node_field} SET type='video_cck' WHERE field_name = '%s'", $field_name);
    $field_name_embed = $field_name . '_embed';
    $field_name_provider = $field_name . '_provider';
    $field_name_data = $field_name . '_data';
    switch ($GLOBALS['db_type']) {
      case 'pgsql':
        $longtext = "text NOT NULL default ''";
        break;
      case 'mysql':
      case 'mysqli':
        $longtext = 'longtext NOT NULL';
        break;
    }
    db_query("ALTER TABLE {" . $table . "} CHANGE COLUMN $field_name_embed $field_name_embed $longtext");
    db_query("ALTER TABLE {" . $table . "} ADD COLUMN $field_name_provider varchar(255) NOT NULL default ''");
    db_query("ALTER TABLE {" . $table . "} ADD COLUMN $field_name_data $longtext");
  }

  // add youtube as provider to existing nodes with embed code/values
  foreach ($field_tables as $field_name => $table) {
    $field_name_provider = $field_name . '_provider';
    $field_name_value = $field_name . '_value';
    $field_name_data = $field_name . '_data';
    db_query("UPDATE {" . $table . "} SET $field_name_provider='youtube', $field_name_data='%s'", serialize(array()));
  }

  // fix views tables
  foreach ($formatters as $old => $new) {
    db_query("UPDATE {view_tablefield} SET options='%s' WHERE options='%s'", $new, $old);
  }

  // set youtube api if we have it (for display of thumbnails)
  if (variable_get('video_cck_youtube_api_key', '') == '') {
    variable_set('video_cck_youtube_api_key', variable_get('youtube_cck_api_key', ''));
  }
  if (variable_get('video_cck_youtube_api_secret', '') == '') {
    variable_set('video_cck_youtube_api_secret', variable_get('youtube_cck_api_secret', ''));
  }

  // clear our content
  db_query("DELETE FROM {cache}");
  if (module_exists('views')) {
    db_query("DELETE FROM {cache_views}");
  }

  // rebuild our content types
  content_enable();
}

